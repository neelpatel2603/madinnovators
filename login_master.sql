-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Oct 16, 2020 at 06:34 PM
-- Server version: 5.7.26
-- PHP Version: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `madinnovators`
--

-- --------------------------------------------------------

--
-- Table structure for table `login_master`
--

DROP TABLE IF EXISTS `login_master`;
CREATE TABLE IF NOT EXISTS `login_master` (
  `lgn_name` varchar(30) NOT NULL,
  `lgn_email` varchar(30) NOT NULL,
  `lgn_pwd` varchar(20) NOT NULL,
  `mobile` bigint(10) NOT NULL,
  PRIMARY KEY (`lgn_email`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login_master`
--

INSERT INTO `login_master` (`lgn_name`, `lgn_email`, `lgn_pwd`, `mobile`) VALUES
('Neel Patel', 'neelptl.2603@gmail.com', '123456789', 8264562728),
('neel', 'neel@gmail.com', '123456789', 9999999999);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
