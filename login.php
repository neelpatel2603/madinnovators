<?php
 error_reporting(0);
 session_start();
include('dbconfig.php');
?>

<html>
<head>

 <link rel="stylesheet" type="text/css" href="lib/jquery/jquery-ui-1.12.1/jquery-ui.min.css">
	<link rel="stylesheet" href="lib/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css">

	<script src="lib/jquery/jquery.min.js" text="javascript" language="javascript"></script>

	<script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="lib/bootstrap/js/bootstrap.min.js"></script>
	<script src="lib/jquery/jquery-ui-1.12.1/jquery-ui.min.js"></script> 

			
	<style>
		.error {
			color: #ff0000;
		}

	</style>
</head>
<body>
<?php
			
		$email = $pwd = "";

		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			
			if ($_POST["email"]) {	
				$email = test_input($_POST["email"]);
			}

			
			if ($_POST["pwd"]) {
				$pwd = test_input($_POST["pwd"]);
			}

            // echo $pwd;
			
				
                $sql="SELECT * FROM `login_master` WHERE lgn_email='$email' AND lgn_pwd='$pwd'";
                $result = mysqli_query($conn,$sql);
                $row = mysqli_fetch_assoc($result);
                
				if ($row>0) {
                    
                        echo '<script>alert("Login successful")</script>';
                        $_SESSION['email'] = $email;
                        header('Location: welcome.php');
                     	
				}
				else{
						echo '<script>alert("Incorrect Email Id or Password")</script>';
				}
                
		
		}

		function test_input($data) {
		$data = trim($data);
		$data = stripslashes($data);
		$data = htmlspecialchars($data);
		return $data;
		}
?>        


	<div class="container">
	<br>  

		<div class="row justify-content-center">
			<div class="col-md-6">
				<div class="card">
					<header class="card-header">
						
						<h4 class="card-title mt-2">LOGIN</h4>
					</header>

					<article class="card-body">
					<form name="login" method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
						
						<div class="form-row">
                            <div class="col-md-7 form-group">
                                <label>Email address </label>
                                <input type="email" name="email" id="email" class="form-control" value="<?php echo $email;?>" placeholder="">
                            </div>
                            
                        </div>
						
						
                        <div class="form-row">
                            <div class="form-group col-md-7">
                                <label>Enter Password </label>   
                                <input class="form-control" name="pwd" id="pwd" value="<?php echo $pwd;?>" type="password">
                            </div> 
                           
                        </div>
						<br>

						<div class="form-row">
						<div class="col-md-2">
						</div>
						<div class="form-group col-md-8">
							<button type="submit" class="btn btn-primary btn-block" id="login" name="login" value="LOGIN" > LOGIN </button>
						</div> <!-- form-group// -->      
					</div>
						
					</form>
					</article>
					<div class="border-top card-body text-center">Don't have an account? <a href="signup.php">Sign Up</a></div>
				</div> 
			</div> 

		</div> 


	</div> 
	
<br><br>

</body>
</html>
