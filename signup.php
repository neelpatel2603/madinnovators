<?php
 error_reporting(0);
 session_start();
include('dbconfig.php');
?>

<html>
<head>
	 <link rel="stylesheet" type="text/css" href="lib/jquery/jquery-ui-1.12.1/jquery-ui.min.css">
	<link rel="stylesheet" href="lib/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css">

	<script src="lib/jquery/jquery.min.js" text="javascript" language="javascript"></script>

	<script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="lib/bootstrap/js/bootstrap.min.js"></script>
	<script src="lib/jquery/jquery-ui-1.12.1/jquery-ui.min.js"></script> 

			
	<style>
		.error {
			color: #ff0000;
		}

	</style>
	
</head>
<body>
	<?php
		// define variables and set to empty values
		$nameErr = $emailErr = $pwdErr = $repwdErr = $mobErr = "";
		$name = $email = $mob = $pwd = $repwd = "";

		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			
			if (empty($_POST["name"])) {
				$nameErr = "Name is required";
			} else {
				$name = test_input($_POST["name"]);
				// check if name only contains letters and whitespace
				if (!preg_match("/^[a-zA-Z-' ]*$/",$name)) {
				$nameErr = "Only letters and white space allowed";
				}
			}
			
			if (empty($_POST["email"])) {
				$emailErr = "Email is required";
			} else {
				$email = test_input($_POST["email"]);
				// check if e-mail address is well-formed
				if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
				$emailErr = "Invalid email format";
				}
			}

			if (empty($_POST["mobile"])) {
				$mobErr = "Mobile no. is required";
			} else {
				$mob = test_input($_POST["mobile"]);
				// check if name only contains numbers and is 10 digits or not
				if (!preg_match("/^[0-9]*$/",$mob)) {
				$mobErr = "Only Numbers are allowed";
				}
				if(strlen((string)$mob)!=10)
				{
					$mobErr = "Only 10 digit Numbers are allowed";
				}
			}
			
			if (empty($_POST["pwd"])) {
				$pwdErr = "Password is required";
			} else {
				$pwd = test_input($_POST["pwd"]);
				//check pwd is 8 characters long or not
				if(strlen((string)$pwd) <= 8){
					$pwdErr = "Password must contain 8 characters";
				}
			}

			if (empty($_POST["re-pwd"])) {
				$repwdErr = "Password verification is required";
			} else {
				$repwd = test_input($_POST["re-pwd"]);
				// check pwd and re-entered pwd matches or not
				if(strcmp($pwd,$repwd)){
					$repwdErr = "Password entered does not match";
				}
			}

			if($nameErr=='' and $emailErr=='' and $mobErr=='' and $pwdErr=='' and $repwdErr=='')
			{
				// $email=mysqli_real_escape_string($conn,$_POST['email']);
				$sql="INSERT INTO login_master (lgn_name, lgn_email, lgn_pwd, mobile) VALUES ('$name' , '$email', '$pwd', $mob)";
				if (mysqli_query($conn, $sql)) {
                     	echo '<script>alert("Registered successfully")</script>';
				}
				else{
						echo '<script>alert("Email ID is already registered with us")</script>';
				}
			}
		
		}

		function test_input($data) {
		$data = trim($data);
		$data = stripslashes($data);
		$data = htmlspecialchars($data);
		return $data;
		}
?>

	<div class="container">
	<br>  

	<div class="row justify-content-center">
		<div class="col-md-6 col-sm-12 col-xs-12">
			<div class="card">
				<header class="card-header">
					
					<h4 class="card-title mt-2">Sign up</h4>
				</header>

				<article class="card-body">
				<form name="registration" method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
					
					<div class="form-row" >
						
						<div class="col-md-7 form-group">
							<label>Name<span class="error ">* </span></label>	  
							<input type="text" name="name" id="name" class="form-control" value="<?php echo $name;?>" placeholder="">
						</div> 
						<div class="col-md-4">
							<br>
							<span class="error "><?php echo $nameErr;?></span>
						</div>
					</div> 

					<div class="form-row">
						<div class="col-md-7 form-group">
							<label>Email address <span class="error ">* </span></label>
							<input type="email" name="email" id="email" class="form-control" value="<?php echo $email;?>" placeholder="">
						</div>
						<div class="col-md-4">
							<br>
							<span class="error "><?php echo $emailErr;?></span>
						</div>
					</div>
					
					<div class="form-row">
						<div class="form-group col-md-7">
							<label>Mobile No. <span class="error ">* </span></label>
							<input type="text" name="mobile" id="mobile" value="<?php echo $mob;?>" class="form-control"> 
						</div>  
						<div class="col-md-4">
							<br>
							<span class="error "><?php echo $mobErr;?></span>
						</div>
					</div> 

					<div class="form-row">
						<div class="form-group col-md-7">
							<label>Create password <span class="error ">* </span></label>   
							<input class="form-control" name="pwd" id="pwd" value="<?php echo $pwd;?>" type="password">
						</div> 
						<div class="col-md-4">
							<br>
							<span class="error "><?php echo $pwdErr;?></span>
						</div>
					</div>
					<div class="form-row">
						<div class="form-group col-md-7">
							<label>Re-enter password <span class="error ">* </span></label>
							<input class="form-control" name="re-pwd" id="re-pwd" value="<?php echo $repwd;?>" type="password">
						</div> 
						<div class="col-md-4">
							<br>
							<span class="error "><?php echo $repwdErr;?></span>
						</div>
					</div> 
					<br>

					<div class="form-row">
						<div class="col-md-2">
						</div>
						<div class="form-group col-md-8">
							<button type="submit" class="btn btn-primary btn-block" id="submit" name="submit" value="Submit" > Register  </button>
						</div> <!-- form-group// -->      
					</div>
					                                         
				</form>
				</article> <!-- card-body end .// -->
				<div class="border-top card-body text-center">Have an account? <a href="login.php">Log In</a></div>
			</div> 
		</div> 

	</div> 


	</div> 
	

<br><br>

</body>


</html>
